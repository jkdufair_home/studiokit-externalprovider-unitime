﻿namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities
{
	public static class UniTimeEndpoints
	{
		private const string BaseUrl = "https://timetable.mypurdue.purdue.edu/Timetabling";

		public static string GetRolesUrl() => $"{BaseUrl}/api/roles";

		public static string GetInstructorTeachingTermsUrl(int instructorId)
			=> $"{BaseUrl}/api/roles?id={instructorId}";

		public static string GetInstructorScheduleUrl(string instructorId, string term)
			=> $"{BaseUrl}/api/instructor-schedule?term={term}&id={instructorId}";

		public static string GetEnrollmentUrl(int classId)
			=> $"{BaseUrl}/api/enrollments?classId={classId}";

		public static string GetCourseSectionUrl(int classId)
			=> $"{BaseUrl}/api/class-info?classId={classId}";
	}
}