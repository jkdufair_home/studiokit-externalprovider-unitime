﻿using Newtonsoft.Json;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ExternalProvider.UniTime.Exceptions;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.TransientFaultHandling.Http;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StudioKit.TransientFaultHandling.Http.Constants;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities
{
	public static class Helpers
	{
		public static string GetCredentials(UniTimeExternalProvider externalProvider)
		{
			var decryptedUsername = EncryptedConfigurationManager.TryDecryptSettingValue(externalProvider.Key);
			var decryptedPassword = EncryptedConfigurationManager.TryDecryptSettingValue(externalProvider.Secret);
			return Base64Encode($"{decryptedUsername}:{decryptedPassword}");
		}

		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			return Convert.ToBase64String(plainTextBytes);
		}

		public static string Base64Decode(string base64EncodedData)
		{
			var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
			return Encoding.UTF8.GetString(base64EncodedBytes);
		}

		#region HttpClient

		public static async Task<T> GetResultAsync<T>(string credentials, string url, CancellationToken cancellationToken, ILogger logger)
		{
			var json = await GetApiResultAsync(credentials, url, cancellationToken, logger);
			return DeserializeResult<T>(json, logger);
		}

		public static async Task<string> GetApiResultAsync(string credentials, string url, CancellationToken cancellationToken, ILogger logger)
		{
			string json;
			var builder = new Uri(url);

			logger.Debug("Getting API result with url {0}", url);
			var client = new RetryingHttpClient();
			try
			{
				json = await client.GetStringAsync(builder, cancellationToken, new AuthenticationHeaderValue(AuthorizationHeaderType.Basic, credentials)).ConfigureAwait(false);
			}
			catch (HttpRequestException ex)
			{
				if (ex.Message.Contains("400"))
					return "";
				throw new UniTimeDownException("UniTime API down", ex);
			}
			logger.Debug("Results fetched from API for {0}", builder.AbsolutePath);
			logger.Debug("Result: {0}", json);
			return json;
		}

		public static T DeserializeResult<T>(string json, ILogger logger)
		{
			logger.Debug("Deserializing json: {0}", json);
			try
			{
				return JsonConvert.DeserializeObject<T>(json);
			}
			catch (JsonReaderException e)
			{
				throw new UniTimeInvalidJsonException($"Invalid UniTime JSON: {json}", e);
			}
		}

		#endregion HttpClient
	}
}