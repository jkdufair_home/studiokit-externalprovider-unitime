﻿using StudioKit.Diagnostics;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Models;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;
using StudioKit.ExternalProvider.UniTime.Exceptions;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.ExternalProvider.UniTime.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Services
{
	public class UniTimeTermProviderService<TExternalTerm> : ITermProviderService<TExternalTerm>
		where TExternalTerm : class, IExternalTerm, new()
	{
		private readonly ILogger _logger;

		public UniTimeTermProviderService(ILogger logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public async Task<IEnumerable<TExternalTerm>> GetTermsAsync(ExternalProvider.Models.ExternalProvider externalProvider, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (!(externalProvider is UniTimeExternalProvider uniTimeProvider))
				throw new ArgumentException(Strings.UniTimeExternalProviderRequired);

			try
			{
				var roles = await Helpers.GetResultAsync<List<Role>>(
					Helpers.GetCredentials(uniTimeProvider),
					UniTimeEndpoints.GetRolesUrl(),
					cancellationToken,
					_logger);
				return roles
					.Where(r => r.Campus == "PWL") // TODO - TermSync: determine how to handle multiple campuses
					.Select(role => new TExternalTerm
					{
						ExternalProviderId = uniTimeProvider.Id,
						ExternalId = $"{role.Term}{role.Year}{role.Campus}",
						Name = $"{role.Term} {role.Year}",
						StartDate = role.EventBeginDate,
						EndDate = role.EventEndDate,
					});
			}
			catch (Exception e)
			{
				throw new UniTimeException(Strings.GetTermsFailed, e);
			}
		}
	}
}