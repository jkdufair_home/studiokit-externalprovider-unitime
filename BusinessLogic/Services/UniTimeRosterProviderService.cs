﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Models;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;
using StudioKit.ExternalProvider.UniTime.Exceptions;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.ExternalProvider.UniTime.Properties;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Services
{
	public class UniTimeRosterProviderService<TUser, TExternalGroup, TExternalGroupUser> : IRosterProviderService<TExternalGroup, TExternalGroupUser>
		where TUser : class, IUser, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		private readonly IErrorHandler _errorHandler;
		private readonly ILogger _logger;
		private readonly IUserInfoService _userInfoService;

		public UniTimeRosterProviderService(IErrorHandler errorHandler, ILogger logger, IUserInfoService userInfoService)
		{
			_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
		}

		public async Task<IEnumerable<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>> GetRosterAsync(ExternalProvider.Models.ExternalProvider externalProvider, TExternalGroup externalGroup, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (!(externalProvider is UniTimeExternalProvider uniTimeProvider))
				throw new ArgumentException(Strings.UniTimeExternalProviderRequired);

			try
			{
				return await GetAllEnrollmentsAsync(uniTimeProvider, externalGroup, cancellationToken);
			}
			catch (Exception e)
			{
				throw new UniTimeException(string.Format(Strings.GetRosterFailed, externalGroup.Id), e);
			}
		}

		/// <summary>
		/// Get a UniTime <see cref="Class"/> represented as an <see cref="TExternalGroup"/>.
		/// </summary>
		/// <param name="externalProvider">The externalProvider</param>
		/// <param name="externalId">The Id of the external group</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>An ExternalGroup</returns>
		public async Task<TExternalGroup> GetExternalGroupAsync(UniTimeExternalProvider externalProvider, int externalId, CancellationToken cancellationToken = default(CancellationToken))
		{
			try
			{
				var courseSection = await Helpers.GetResultAsync<Class>(
					Helpers.GetCredentials(externalProvider),
					UniTimeEndpoints.GetCourseSectionUrl(externalId),
					cancellationToken,
					_logger);
				return CreateExternalGroup(externalProvider, courseSection);
			}
			catch (Exception ex)
			{
				throw new UniTimeException(string.Format(Strings.GetExternalGroupFailed, externalId), ex);
			}
		}

		/// <summary>
		/// Get an instructor's UniTime Schedule of <see cref="Class"/>s represented as <see cref="TExternalGroup"/>s.
		/// </summary>
		/// <param name="externalProvider">The externalProvider</param>
		/// <param name="instructorId">The Id of the instructor in UniTime, e.g. PUID without leading 0s</param>
		/// <param name="termReference">The UniTime term reference</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>A list of ExternalGroups</returns>
		public async Task<IEnumerable<TExternalGroup>> GetInstructorScheduleAsync(UniTimeExternalProvider externalProvider, string instructorId, string termReference, CancellationToken cancellationToken = default(CancellationToken))
		{
			try
			{
				var instructorSchedule = await Helpers.GetResultAsync<InstructorSchedule>(
					Helpers.GetCredentials(externalProvider),
					UniTimeEndpoints.GetInstructorScheduleUrl(instructorId, termReference),
					cancellationToken,
					_logger);
				return instructorSchedule?.Classes
							.GroupBy(g => g.ClassId)
							.Select(s => s.First())
							.Select(cs => CreateExternalGroup(externalProvider, cs))
						?? new List<TExternalGroup>();
			}
			catch (Exception ex)
			{
				throw new UniTimeException(string.Format(Strings.GetInstructorScheduleFailed, instructorId, termReference), ex);
			}
		}

		#region Private Methods

		private async Task<IEnumerable<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>> GetAllEnrollmentsAsync(UniTimeExternalProvider externalProvider, TExternalGroup externalGroup, CancellationToken cancellationToken = default(CancellationToken))
		{
			var roster = new List<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>();
			var enrollments = await Helpers.GetResultAsync<IEnumerable<Enrollment>>(Helpers.GetCredentials(externalProvider), UniTimeEndpoints.GetEnrollmentUrl(Convert.ToInt32(externalGroup.ExternalId)), cancellationToken, _logger);
			var enrollmentList = enrollments?.ToList() ?? new List<Enrollment>();
			foreach (var enrollment in enrollmentList)
			{
				// only add once
				if (roster.Any(e =>
					e.ExternalGroupUser.ExternalGroupId == externalGroup.Id &&
					e.ExternalGroupUser.ExternalUserId == enrollment.ExternalId)) continue;
				roster.Add(new ExternalRosterEntry<TExternalGroup, TExternalGroupUser>
				{
					ExternalGroupUser = new TExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = enrollment.ExternalId,
						User = new TUser
						{
							UserName = enrollment.Email,
							Email = enrollment.Email,
							FirstName = enrollment.FirstName,
							LastName = enrollment.LastName,
							EmployeeNumber = enrollment.ExternalId.Length < 10 ? enrollment.ExternalId.PadLeft(10, '0') : enrollment.ExternalId,
							Uid = enrollment.Email?.Substring(0, enrollment.Email.IndexOf("@", StringComparison.Ordinal))
						}
					},
					GroupUserRoles = new List<string>
					{
						BaseRole.GroupLearner
					}
				});
			}

			// handle UniTime users that have no Email, update from UserInfo
			var entriesMissingEmail = roster.Where(e => e.ExternalGroupUser.User.Email == null).ToList();
			if (!entriesMissingEmail.Any()) return roster;

			var invalidUserIdentifiers = entriesMissingEmail
				.Select(e => e.ExternalGroupUser.User.EmployeeNumber)
				.ToList();
			var (userInfoUsers, invalidIdentifiers) = _userInfoService.GetUserInfoUsersByIdentifiers<TUser>(invalidUserIdentifiers);
			if (invalidIdentifiers.Any())
			{
				var message = string.Format(Strings.UserInfoMissingEmail, string.Join(",", invalidIdentifiers));
				_logger.Error(message);
				_errorHandler.CaptureException(new Exception(message));
			}

			// filter and update entries without email with UserInfo
			var entriesFixed = entriesMissingEmail
				.Where(e => userInfoUsers.Any(u => e.ExternalGroupUser.User.EmployeeNumber == u.EmployeeNumber))
				.Select(e =>
				{
					e.ExternalGroupUser.User = userInfoUsers.First(u => e.ExternalGroupUser.User.EmployeeNumber == u.EmployeeNumber);
					return e;
				})
				.ToList();

			return roster.Except(entriesMissingEmail).Concat(entriesFixed).ToList();
		}

		private static TExternalGroup CreateExternalGroup(ModelBase externalProvider, Class @class)
		{
			// TODO - RosterSyncRefactor: determine best naming convention for cross-listed courses
			var courseNames = @class.Course.Select(c => $"{c.SubjectArea} {c.CourseNumber} {@class.SectionNumber}");
			var courseNamesAndTitles = @class.Course.Select(c => $"{c.SubjectArea} {c.CourseNumber} {@class.SectionNumber} - {c.CourseTitle} - {@class.Fullpart}");
			return new TExternalGroup
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = @class.ClassId.ToString(),
				Name = string.Join(", ", courseNames),
				Description = string.Join(", ", courseNamesAndTitles)
			};
		}

		#endregion Private Methods
	}
}