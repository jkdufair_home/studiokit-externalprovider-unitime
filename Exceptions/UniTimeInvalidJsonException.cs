﻿using System;

namespace StudioKit.ExternalProvider.UniTime.Exceptions
{
	public class UniTimeInvalidJsonException : Exception
	{
		public UniTimeInvalidJsonException()
		{
		}

		public UniTimeInvalidJsonException(string message)
			: base(message) { }

		public UniTimeInvalidJsonException(string message, Exception inner)
			: base(message, inner) { }
	}
}